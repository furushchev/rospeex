#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import logging
import unittest

# import pypi libraries
from nose.tools import raises

# import local libraries
from rospeex_core.sr import Factory
from rospeex_core import exceptions as ext

# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestSpeechRecognitionFactory(unittest.TestCase):
    def setUp(self):
        pass

    @raises(ext.SpeechRecognitionException)
    def test_create_invalid_engine(self):
        Factory.create('hoge')

    def test_create_valid_engine(self):
        for engine in Factory.ENGINE_FACTORY.keys():
            Factory.create(engine)


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(
        'rospeex_core',
        'speech_synthesis_factory',
        TestSpeechRecognitionFactory,
        None,
        coverage_packages=['rospeex_core.sr']
    )
