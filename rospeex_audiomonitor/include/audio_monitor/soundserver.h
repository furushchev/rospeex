#ifndef SOUNDSERVER_H
#define SOUNDSERVER_H

#include <QTcpServer>
#include <QList>
#include "serversocket.h"

class SoundServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit SoundServer(QObject *parent = 0);

    void SendStart();
    void SendData( const void *data, int len );
    void SendEnd();
    void SendCancel();
    void SendEpdOn();
    void SendEpdOff();
    void SendKeyPressed( int val );

protected:
    virtual void incomingConnection(int socketDescriptor);

signals:
    void onCmd(const QString &cmd);
    void clientConnected(const QHostAddress &addr);

private slots:
    void socketFinished();
    void read(const QString &cmd);

private:
    QList<ServerSocket*> sockets;
};

#endif // SOUNDSERVER_H
