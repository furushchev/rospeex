#ifndef WAVEFILE_H
#define WAVEFILE_H

#include <QString>
#include <QFile>

class WaveFile
{
private:
    WaveFile();

public:
    static QFile* createHeader(const QString &filename);
    static void fillHeader(QFile *file, int sampleSize);

    static int SAMPLING_RATE;
    static int SAMPLE_BYTE;
};

#endif // WAVEFILE_H
