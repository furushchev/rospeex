cmake_minimum_required(VERSION 2.8.3)
project(rospeex_samples)

find_package(catkin REQUIRED COMPONENTS
  rospy
  roscpp
  rospeex_msgs
  rospeex_if
  roslib
)

find_package(Boost REQUIRED COMPONENTS system)

catkin_package(
  CATKIN_DEPENDS roscpp rospy std_msgs rospeex_msgs rospeex_if roslib
  DEPENDS system_lib
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

## Declare a cpp executable
add_executable(talking_watch_sample src/cpp/talking_watch_sample.cpp)
add_executable(talking_watch_sample_en src/cpp/talking_watch_sample_en.cpp)

add_dependencies(talking_watch_sample ${catkin_EXPORTED_TARGETS})
add_dependencies(talking_watch_sample_en ${catkin_EXPORTED_TARGETS})

target_link_libraries(talking_watch_sample ${catkin_LIBRARIES})
target_link_libraries(talking_watch_sample_en ${catkin_LIBRARIES})

install(PROGRAMS
  src/py/talking_watch_sample.py
  src/py/talking_watch_sample_en.py
  src/py/turtle_demo.py
  src/py/turtle_demo_en.py
  PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY launch/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
  PATTERN ".svn" EXCLUDE
)

install(TARGETS talking_watch_sample talking_watch_sample_en
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
