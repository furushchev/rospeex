# -*- coding: utf-8 -*-

# pypi libraries
import tornado.ioloop
import tornado.web
import tornado.websocket
from tornado.ioloop import PeriodicCallback

# local libraries
import logging_util

__all__ = [
    'ModWebSocketHandler',
]

# create logger
logger = logging_util.get_logger(__file__)


class ModWebSocketProtocol8(tornado.websocket.WebSocketProtocol8):
    def _handle_message(self, opcode, data):
        if self.client_terminated:
            return

        if opcode == 0xA:
            self.async_callback(self.handler.on_pong)(data)

        super(ModWebSocketProtocol8, self)._handle_message(opcode, data)


class ModWebSocketHandler(tornado.websocket.WebSocketHandler):
    """
    """
    def write_message(self, message, binary=False):
        self.ws_connection.write_message(message, binary)

    def _execute(self, transform, *args, **kwargs):
        try:
            self.open_args = args
            self.open_kwargs = kwargs

            version = self.request.headers.get('Sec-WebSocket-Version')
            if version in ('8', '7', '13'):
                self.ws_connection = ModWebSocketProtocol8(self)
                self.ws_connection.accept_connection()

            elif version:
                self.stream.write(tornado.escape.utf8(
                    "HTTP/1.1 426 Upgrade Required\r\n"
                    "Sec-WebSocket-Version: 8\r\n\r\n"))
                self.stream.close()

            else:
                self.ws_connection = tornado.websocket.WebSocketProtocol76(self)
                self.ws_connection.accept_connection()

        except Exception as err:
            logger.err(err)

    def ping(self, data):
        if not self.ws_connection.client_terminated:
            self.ws_connection._write_frame(True, 0x9, data)
